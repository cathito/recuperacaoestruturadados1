package Questao2;

public class LDEC {

    private No inicio;
    int qtdListas=0;

    public void AdicionarInicio(int valor) {
        No novo = new No(valor);
        
        if(inicio==null ) {
            novo.anterior=novo;
            novo.proximo=novo;
        }else {
            novo.anterior=inicio.anterior;
            inicio.anterior.proximo=novo;
            inicio.anterior=novo;
            novo.proximo=inicio;
            
        }
        inicio=novo;
        qtdListas++;

    }
//--------------------------------------------------------------------------//

    public void removerTudo(){

        inicio.anterior=null;
        inicio.proximo=null;
        inicio=null;
    }
//================================================================================//

    public void ordenar(boolean crescente) throws Exception{
        int [] copia = new int[qtdListas];
        for(int i=0; i<qtdListas;i++){
            copia[i]=inicio.dado;
            inicio=inicio.proximo;
        }

        if(crescente){
            System.out.println("            Lista Ordenada em Forma Crescente!");
            MergeCrescente md= new MergeCrescente();
            md.MergeQuebra(copia);
        }else{
            System.out.println("            Lista Ordenada em Forma Decrescente!");
            MergeDecrescente m = new MergeDecrescente();
            m.MergeQuebra(copia);
        }
        removerTudo();

        System.out.println(" Tamanho copia: "+ copia.length);
        for(int i=0; i<copia.length;i++){
            AdicionarInicio(copia[i]);
        }



    }
//================================================================================//
    public void Listar() {
    	No novo= inicio;
    	int pulo=0;
        int contadorLup=0;

        System.out.println("\nPrintando Lista: ");
        if(novo==null) {
            System.out.println("\n Lista Vazia!");
            return;
        }
        while(true) {
        	
            if( pulo==qtdListas){
            	System.out.print(" //  ");
            	pulo=0;
            }
            if (contadorLup==3*qtdListas)
            	break;
            
            System.out.print(novo.dado+", ");
            novo=novo.proximo;
            contadorLup++;
            pulo++;
        }
        //System.out.println("\nTamanho da Lista: "+this.qtdListas);
    }
//--------------------------------------------------------------------------//



}
