package Questao1;

import java.util.Random;

public class QuickSort implements Comparable{

	Random random = new Random();

	public String[] MergeQuebra(String[] Original) {
		
		if(Original.length==1) {
			return Original;
		}
		//int Pivo=Original.length/2;

		int Pivo= random.nextInt(Original.length);
		while(Pivo<=0){
			Pivo= random.nextInt(Original.length);
		}

		String[] aux = new String[Original.length];
		String[] Fatia1=new String[Pivo];//esquerda
		String[] Fatia2=new String[Original.length-Pivo];//direita
		
		for (int i = 0; i < Original.length; i++) {

			if (i<Pivo) {
				Fatia1[i]=Original[i];
			}else {
				Fatia2[i-Pivo]=Original[i];
			}
		}
		Fatia1 = MergeQuebra(Fatia1);
		Fatia2= MergeQuebra(Fatia2);
		
		aux=Juncao(Original,Fatia1, Fatia2);
		
		return aux;
		
	}
//===================================================================================//
	
	private String[] Juncao(String[] original, String[] Fatia1, String[] Fatia2) {
		
		int ContadorOriginal=0;
		int ContadorFatia1=0;
		int ContadorFatia2=0;
	//----------------------------------------------------------------------------//
		while(ContadorFatia1<Fatia1.length && ContadorFatia2<Fatia2.length) {

			if (Fatia1[ContadorFatia1].compareTo(Fatia2[ContadorFatia2])>0){// <= Fatia2[ContadorFatia2]) {
				original[ContadorOriginal]=Fatia1[ContadorFatia1];
				ContadorFatia1++;
			}
			else {
				original[ContadorOriginal]=Fatia2[ContadorFatia2];
				ContadorFatia2++;
			}
			ContadorOriginal++;
		}
		while(ContadorFatia1<Fatia1.length) {
			original[ContadorOriginal]=Fatia1[ContadorFatia1];
			ContadorFatia1++;
			ContadorOriginal++;
		}while(ContadorFatia2<Fatia2.length) {
			original[ContadorOriginal]=Fatia2[ContadorFatia2];
			ContadorFatia2++;
			ContadorOriginal++;
		}
		return original;
	}


	@Override
	public int compareTo(Object o) {
		return 0;
	}
}
