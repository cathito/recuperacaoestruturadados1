package Questao2;

public class MergeCrescente {
	public int[] MergeQuebra(int[] Original) {
		
		if(Original.length==1) {
			return Original;
		}
		int Pivo=Original.length/2;
		int[] aux = new int[Original.length];
		int[] Fatia1=new int[Pivo];//esquerda
		int[] Fatia2=new int[Original.length-Pivo];//direita
		
		for (int i = 0; i < Original.length; i++) {
			if (i<Pivo) {
				Fatia1[i]=Original[i];
			}else {
				Fatia2[i-Pivo]=Original[i];
			}
		}
		
		
		Fatia1 = MergeQuebra(Fatia1);
		Fatia2= MergeQuebra(Fatia2);
		
		//printarFatias(Fatia1, Fatia2);
		
		aux=Juncao(Original,Fatia1, Fatia2);
		
		return aux;
		
	}
	
	
//-----------------------------------------------------------------------//
	public void printarFatias(int[] Fatia1,int[] Fatia2) {
		for (int i = 0; i < Fatia1.length; i++) {
			System.out.print(Fatia1[i]+", ");
		}
		System.out.println("\n-------------------");
		for (int i = 0; i < Fatia2.length; i++) {
			System.out.print(Fatia2[i]+ ", ");
		}
	}
//===================================================================================//
	
	private int[] Juncao(int[] original, int[] Fatia1, int[] Fatia2) {
		
		//int [] Original=new int[Fatia1.length+Fatia2.length];
		
		int ContadorOriginal=0;
		int ContadorFatia1=0;
		int ContadorFatia2=0;
	//----------------------------------------------------------------------------//
		while(ContadorFatia1<Fatia1.length && ContadorFatia2<Fatia2.length) {
			
			if (Fatia1[ContadorFatia1] >= Fatia2[ContadorFatia2]) {
				original[ContadorOriginal]=Fatia1[ContadorFatia1];
				ContadorFatia1++;
			}
			else {
				original[ContadorOriginal]=Fatia2[ContadorFatia2];
				ContadorFatia2++;
			}
			ContadorOriginal++;
		}
		while(ContadorFatia1<Fatia1.length) {
			original[ContadorOriginal]=Fatia1[ContadorFatia1];
			ContadorFatia1++;
			ContadorOriginal++;
		}while(ContadorFatia2<Fatia2.length) {
			original[ContadorOriginal]=Fatia2[ContadorFatia2];
			ContadorFatia2++;
			ContadorOriginal++;
		}
		return original;
	}
	
	

}
