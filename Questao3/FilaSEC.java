package Questao3;

public class FilaSEC {
	
	private No inicio;
	private No fim;
	int qtdListas=0;
//----------------------------------------------------//
	
	public void Adicionar(int valor) {
		No novo = new No(valor);
		
		if(inicio==null ) {
			inicio=novo;
			inicio.proximo=novo;
			fim=novo;
			fim.proximo=inicio;
		}else {
			novo.proximo=inicio;
			fim.proximo=novo;
			fim=novo;
		}
		qtdListas++;
	}
//----------------------------------------------------//
	
	
	public void Remover() {
		//System.out.println("\nretirando...");
		No atual=inicio.proximo;
		inicio=atual;
		fim.proximo=inicio;
		qtdListas--;

	}
	
//----------------------------------------------------//
	
	public void Listar() {
    	No novo= inicio;
    	int pulo=0;
        int contadorLup=0;

        System.out.println("\nPrintando Lista: ");
        
        while(true) {
        	
            if( pulo==qtdListas){
            	System.out.print(" //  ");
            	pulo=0;
            }
            if (contadorLup==3*qtdListas)
            	break;
            
            System.out.print(novo.dado+", ");
            novo=novo.proximo;
            contadorLup++;
            pulo++;
        }
        System.out.println("\nTamanho da Lista: "+this.qtdListas);
    }
	
	
	
	
	
	
}
