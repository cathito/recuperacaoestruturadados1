package Questao1;


public class TesteQuick {

	public static void Print(String[] Original) {
		for (int i = 0; i < Original.length; i++) {
			System.out.print(Original[i]+", ");
		}
	}
	
	public static void main(String[] args) {
		String [] Original= {"ana","eduardo","anao", "daniel", "gaby", "bruno","wesley", "joao"};
		System.out.println("Vetor Original!");
		Print(Original);


		QuickSort m= new QuickSort();
		m.MergeQuebra(Original);
		
		System.out.println("\n\nVetor Ordenado em Ordem Decrescente!");
		Print(Original);

	}

}
